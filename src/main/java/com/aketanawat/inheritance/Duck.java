/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.inheritance;

/**
 *
 * @author Acer
 */
public class Duck extends Animal{
    private int numberOfWings;
    public Duck(String name ,String color){
        super (name,color,2);
        System.out.println("Duck created");
    }
    public void fly(){
        System.out.println("Duck: "+this.name +" FLY! ");
    }  
    @Override
    public void walk(){
        System.out.println("Duck: "+ name +  " walk with "+numberOfLegs +" Legs." );
    }
    @Override
    public void speak(){
        System.out.println("Duck: "+name+" speak > Garp Garp!");
}
}
